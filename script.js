const hamburgerIcon = document.getElementsByClassName('hamburger-icon')[0];
const closeIcon = document.getElementsByClassName('close-icon')[0];
const menuList = document.getElementsByClassName('header-menu')[0];
const body = document.getElementsByTagName('body')[0]; 

hamburgerIcon.addEventListener('click', () => {
    menuList.style.display = 'flex';
    hamburgerIcon.style.display = 'none';
    closeIcon.style.display = 'inline-block';
    body.style.overflow = 'hidden';
    
}); 

closeIcon.addEventListener('click', () => {
    menuList.style.display = 'none';
    hamburgerIcon.style.display = 'inline-block';
    closeIcon.style.display = 'none';
    body.style.overflow = 'auto';
});

window.onclick = function(event){
    if(event.target == menuList){
        closeIcon.click();
    }
};